package app.mobile.ecommerce.ecommerce.product;

import org.springframework.data.repository.CrudRepository;

import app.mobile.ecommerce.ecommerce.model.Product;

public interface ProductRepository extends CrudRepository<Product,Integer>{

}
